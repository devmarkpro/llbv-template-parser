package templateparser

// Snapshot is the moment during the video that question comes up
type Snapshot struct {
	ID           string       `json:"id"`
	FromSeconds  int64        `json:"from"`
	ToSeconds    int64        `json:"to"`
	QuestionType QuestionType `json:"questionType"`
	Options      []Options    `json:"options"`
}

// Options is the available options for each question
type Options struct {
	ID   string `json:"id"`
	Text string `json:"body"`
}
