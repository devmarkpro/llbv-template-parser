package templateparser

import (
	"encoding/json"
	"fmt"
)

// TemplateParser is the main interface
type TemplateParser interface {
	Validate(body []byte) error
	Parse(body []byte) (*Template, error)
}

// LLBVParser is the main parser
type LLBVParser struct{}

// Validate run some validation to the input to ensure it follow the rules of the template
func (p LLBVParser) Validate(body []byte) error {
	var tmp Template
	if err := json.Unmarshal(body, &tmp); err != nil {
		return err
	}
	if len(tmp.Meta.ID) == 0 {
		return fmt.Errorf("Meta must be have ID")
	}
	return nil
}

// Parse string and get template
func (p LLBVParser) Parse(body []byte) (*Template, error) {
	var tmp Template
	if err := json.Unmarshal(body, &tmp); err != nil {
		return nil, err
	}

	return &tmp, nil
}
