package templateparser

type Template struct {
	Meta      Metadata   `json:"meta"`
	Snapshots []Snapshot `json:"snapshots"`
}
